# Aruna Projects

- [url](aruna-next.vercel.app)

## How To Install NextJS

```
npx create-next-app@latest
```

### Configuration

- What is your project named? aruna
- Would you like to use TypeScript? Yes
- Would you like to use ESLint? Yes
- Would you like to use Tailwind CSS? Yes
- Would you like to use `src/` directory? No
- Would you like to use App Router? (recommended) Yes
- Would you like to customize the default import alias (`@/*`)? Yes
- What import alias would you like configured? `@/*`

## Install Shadcn UI

```
npx shadcn-ui@latest init
```

### Configuration

- Which style would you like to use? › Default
- Which color would you like to use as base color? › Slate
- Would you like to use CSS variables for colors? yes
