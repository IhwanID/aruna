import { Button } from "@/components/ui/button"
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar"
import { buttonVariants } from "@/components/ui/button"
import { Separator } from "@/components/ui/separator"
import { Github, Menu } from "lucide-react"
import { Youtube, Linkedin, Mail, Twitter } from "lucide-react"
import {
  Sheet,
  SheetContent,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet"
import { Label } from "@/components/ui/label"
import Link from "next/link"


export default function Home() {
  return (
    <main className="flex flex-col p-4 justify-between h-dvh">
      <div>
        <Sheet>
          <SheetTrigger asChild>
            <div className="relative flex justify-end">
              <Button variant="ghost" className="fixed top-0 right-0 m-4">
                <Menu className="mr-2 h-4 w-4" /> Menu
              </Button>
            </div>
          </SheetTrigger>
          <SheetContent>
            <SheetHeader>
              <SheetTitle>Menu</SheetTitle>
            </SheetHeader>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-4 items-center gap-4">
                <h3>
                  Home
                </h3>
              </div>
              <div className="grid grid-cols-4 items-center gap-4">
                <h3>Tools</h3>
              </div>
            </div>
          </SheetContent>
        </Sheet>
        <div className="flex items-center mt-4">
          <Avatar className="w-20 h-20">
            <AvatarImage src="https://github.com/ihwanid.png" />
            <AvatarFallback>Ihwan</AvatarFallback>
          </Avatar>
          <h2 className="ml-4 text-2xl text-black font-bold">Ihwan D</h2>
        </div>
        <Separator className="my-4" />
        <div>
          <Label className="text-lg">17 year old programmer 👨🏻‍💻 based in Majalengka 🇮🇩 making videos on Youtube under the name Ihwan.</Label>
          <div>
            <Button variant="link" className="p-0 font-semibold text-blue-500 text-lg ">Interesting in working together? Email me!</Button>

          </div>
        </div>
        <Separator className="my-4" />
      </div>
      <div>

        <div className="flex justify-evenly">
          <Button variant="outline" size="icon" asChild >
            <Link href="https://youtube.com/codewithihwan">
              <Youtube className="h-4 w-4" />
            </Link>
          </Button>
          <Button variant="outline" size="icon">
            <Link href="https://linkedin.com/ihwanid">
              <Linkedin className="h-4 w-4" />
            </Link>
          </Button>
          <Button variant="outline" size="icon">
            <Link href="https://x.com/ihwan_id">
              <Twitter className="h-4 w-4" /></Link>
          </Button>
          <Button variant="outline" size="icon">
            <Github className="h-4 w-4" />
          </Button>
        </div>

        <div className="pt-4 flex justify-center">
          <p>
            © 2024 Ihwan D. All rights reserved.
          </p>
        </div>
      </div>
    </main>
  );
}
